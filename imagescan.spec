%{!?upstream:   %define upstream utsushi}
%{!?uversion:   %define uversion 0.65.0}

Name:           imagescan
Version:        3.65.0
Release:        1%{?dist}
Summary:        Next Generation Image Acquisition Utilities

Vendor:         SEIKO EPSON CORPORATION
License:        GPLv3+
URL:            http://download.ebz.epson.net/dsc/search/01/search/?OSC=LX
Source0:        https://support.epson.net/linux/src/scanner/imagescanv3/common/imagescan_%{version}.orig.tar.gz
Source1:        %{name}.desktop
Source2:        %{name}.conf
Patch0:         rebranding.patch
Patch1:         gtkmm-2.20-compat.patch
Patch2:         gdk-pixbuf-format.patch
Patch3:         utsushi.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%define Magick  %{?centos:Image}%{!?centos:Graphics}Magick

Requires:       %{Magick}

BuildRequires:  gcc-c++
BuildRequires:  desktop-file-utils, pkgconfig, libtool-ltdl-devel
BuildRequires:  gtkmm24-devel, sane-backends-devel, libjpeg-devel
BuildRequires:  libtiff-devel, %{Magick}

%if 06 == 0%{?centos}
BuildRequires:  bzip2-devel, zlib-devel
%else
# openSUSE 42.2 specific code
# refer https://en.opensuse.org/openSUSE:Build_Service_cross_distribution_howto#Detect_a_distribution_flavor_for_special_code
%if 1315 == 0%{?suse_version} && 120200 == 0%{?sle_version}
BuildRequires:  boost_1_58_0-devel, gcc5-c++
%else
BuildRequires:  boost-devel
%endif
%endif

%if 06 == 0%{?centos} || 0%{?suse_check:1}
BuildRequires:  libudev-devel
%else
BuildRequires:  systemd-devel
%endif

%if 0%{?suse_check:1}
BuildRequires:  libusb-1_0-devel, libGraphicsMagick++-devel
%else
BuildRequires:  libusb1-devel, %{Magick}-c++-devel
%endif

%description
This software provides applications to easily turn hard-copy documents
and imagery into formats that are more amenable to computer processing.

Included are a native driver for a number of EPSON scanners and a
compatibility driver to interface with software built around the SANE
standard.


%if ! 0%{?_enable_debug_packages}
%debug_package
%endif


%prep
%setup -q -n %{upstream}-%{uversion}
%patch -P 0 -p1
%patch -P 3 -p1
sed -i 's/^utsushi-0\.\([^ \t]*\)  /imagescan-3.\1/' NEWS

# Upstream requires gtkmm-2.20 or later but CentOS 6 only has 2.18.
# Apply a backward compatibility patch and make configure accept the
# version that CentOS 6 ships.  Avoid the need for running autotools
# as that is likely to fail due to their version requirements.  Also
# apply a patch to work around a GDK pixbuf image format recognition
# issue.
#
%if 06 == 0%{?centos}
%patch -P 1 -p1
sed -i 's/\(gtkmm-2\.4 >= 2\)\.20/\1.18/g' configure
%patch -P 2 -p1
%endif

%if 023 <= 0%{?fedora}
%define CXX CXX='%{__cxx} -std=c++11'
%else
%if 1315 == 0%{?suse_version} && 120200 == 0%{?sle_version}
%define CXX CXX='%{__cxx}-5 -std=c++11'
%endif
%endif

%if 024 <= 0%{?fedora}
%define udev_d %(pkg-config --variable=udevdir udev)
%define with_udev_d --with-udev-confdir=%{udev_d}
%endif

%if 4230 == 0%{?suse_version}
%define CXX CXX='%{__cxx}-5 -std=c++11'
%endif

%build
%configure \
    --with-jpeg \
    --with-tiff \
    --with-gtkmm \
    --with-sane \
    --with-magick \
    --with-magick-pp \
    --disable-static \
    %{?with_udev_d} \
    %{?CXX}
make %{?_smp_mflags} BACKEND_NAME=%{name}

%install
rm -rf %{buildroot}
make install BACKEND_NAME=%{name} DESTDIR=%{buildroot}
desktop-file-install \
    --dir=%{buildroot}%{_datadir}/applications \
    %{SOURCE1}
test -d %{buildroot}%{_sysconfdir}/%{name} \
    || %{__mkdir_p} %{buildroot}%{_sysconfdir}/%{name}
install -m 644 %{SOURCE2} \
    %{buildroot}%{_sysconfdir}/%{name}/%{name}.conf
mv %{buildroot}%{_sysconfdir}/%{upstream}/combo.conf \
   %{buildroot}%{_sysconfdir}/%{name}
mv %{buildroot}%{_bindir}/%{upstream} %{buildroot}%{_bindir}/%{name}
%if 0%{?udev_d:1}
mv %{buildroot}%{udev_d}/rules.d/%{upstream}-esci.rules \
   %{buildroot}%{udev_d}/rules.d/70-%{name}.rules
%endif
rm -rf %{buildroot}%{_includedir}
rm -rf %{buildroot}%{_libdir}/lib*.a
rm -rf %{buildroot}%{_libdir}/lib*.la
rm -rf %{buildroot}%{_libdir}/lib*.so
rm -rf %{buildroot}%{_libdir}/%{upstream}/lib*.a
rm -rf %{buildroot}%{_libdir}/%{upstream}/lib*.so
rm -rf %{buildroot}%{_libdir}/%{upstream}/sane/lib*.a
rm -rf %{buildroot}%{_libdir}/%{upstream}/sane/lib*.la
rm -rf %{buildroot}%{_libdir}/%{upstream}/sane/lib*.so
rm -rf %{buildroot}%{_libdir}/sane/lib*.a
rm -rf %{buildroot}%{_libdir}/sane/lib*.la
rm -rf %{buildroot}%{_libdir}/sane/lib*.so
%find_lang %{upstream}

%clean
rm -rf %{buildroot}


%define have_sane_dll_d %(test -d %{_sysconfdir}/sane.d/dll.d && echo true)

%files -f %{upstream}.lang
%defattr(-,root,root,-)
%doc NEWS README AUTHORS
%doc COPYING
%{_bindir}/%{name}
%{_libdir}/%{upstream}/
%if "%{_libdir}" != "%{_libexecdir}"
%{_libexecdir}/%{upstream}/
%endif
%{_libdir}/sane/libsane-%{name}.*
%{_datadir}/applications/%{name}.desktop
%{_datadir}/%{upstream}/
%config(noreplace) %{_sysconfdir}/%{name}/combo.conf
%config(noreplace) %{_sysconfdir}/%{name}/%{name}.conf
%if "true" == "%{have_sane_dll_d}"
%{_sysconfdir}/sane.d/dll.d/%{name}
%endif
%if 0%{?udev_d:1}
%{udev_d}/rules.d/70-%{name}.rules
%else
%{_sysconfdir}/udev/rules.d/%{upstream}-esci.rules
%endif


%if "true" != "%{have_sane_dll_d}"
#
#  Modify SANE's dll.conf during (un)installation
#
%post
dll=%{_sysconfdir}/sane.d/dll.conf
if [ -n "`grep '^[ \t]*#[ \t#]*%{name}' ${dll}`" ]
then                            # uncomment existing entry
    sed -i 's,^[ \t]*#[ \t#]*\(%{name}\),\1,' ${dll}
elif [ -z "`grep %{name} ${dll}`" ]
then                            # append brand new entry
    echo %{name} >> ${dll}
fi

%preun
if [ $1 = 0 ]
then                            # comment out existing entry
    dll=%{_sysconfdir}/sane.d/dll.conf
    if [ -n "`grep '^[ \t]*%{name}' ${dll}`" ]
    then
        sed -i 's,^[ \t]*\(%{name}\),#\1,' ${dll}
    fi
fi
%endif


%changelog
* Thu Dec 22 2020 Seiko Epson <linux-printer@epson.jp> - 3.65.0-1
- new upstream
